from django.urls import path
from .views import (
    api_list_technicians,
    api_show_technicians,
    api_list_appointments,
    api_appointments,
    cancel_appointment,
    finish_appointment,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_technicians, name="api_show_technicans"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", finish_appointment, name="finish_appointment"),
]
