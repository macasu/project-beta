from django.db import models

# Create your models here.# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)
    # def __str__(self):
    #     #return self.first_name +''+ self.last_name
    #     return f"{self.first_name} {self.last_name}"
    def __str__(self):
        return self.employee_id

class Customer(models.Model):
    first_name =  models.CharField(max_length=200)
    last_name =  models.CharField(max_length=200)
    address =  models.CharField(max_length=200)
    phone_number=  models.PositiveIntegerField()

    def __str__(self):
        #return self.first_name +''+ self.last_name
        return f"{self.first_name} {self.last_name} {self.address} {self.phone_number}"

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="automobile", on_delete=models.CASCADE
        )
    salesperson = models.ForeignKey(
        SalesPerson, related_name="salespeople", on_delete=models.CASCADE
        # might need to change related name to sales
        )
    customer = models.ForeignKey(
        Customer, related_name="customers",on_delete=models.CASCADE)
    price = models.CharField(max_length=200)
