from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from .models import AutomobileVO, SalesPerson, Sale, Customer
import json

from common.json import ModelEncoder


# Create your views here.
class AutoMobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]

class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price"
    ]
    encoders = {"automobile": AutoMobileVOEncoder(), "salesperson": SalesPersonListEncoder(), "customer": CustomerListEncoder()}
    # def get_extra_dastqa(self,o):
    #     return {}

class SalesDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "salesperson",
        "customer",
        "price"
    ]
    encoders = {"automobile": AutoMobileVOEncoder(), "salesperson": SalesPersonListEncoder(), "customer": CustomerListEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        salesperson = SalesPerson.objects.all()
        #print("current salesperson")
        return JsonResponse(
            {"salespeople": salesperson},
            encoder=SalesPersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales person"}
            )
            response.status_code = 400
            return response


@require_http_methods([ "GET", "DELETE"])
def api_show_sales_people(request, pk):
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
                )
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count>0})
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

# FINISH LIST_CUSTOMERS
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder= CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods([ "GET", "DELETE"])
def api_show_customers(request, pk):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=pk)
            return JsonResponse(
                customers,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
                )
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count>0})
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        #print("current salesperson")
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=vin)
            content['automobile'] = automobile
            salesperson_id = content["salesperson_id"]
            salesperson = SalesPerson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            print(content)
            sales = Sale.objects.create(**content)
            AutomobileVO.objects.filter(vin=vin).update(sold=False)
            return JsonResponse(
                sales,
                encoder=SalesListEncoder,
                safe=False,
            )
#         except Exception as e:
#             # print(f"Vin value from request: {vin}")
#             response = JsonResponse(
#                 {"message": "Could not create the sale" + str(e)}
#             )
#             response.status_code = 400
#             return response
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
            {"message": "Could not create the sale - AutomobileVO with VIN does not exist"}
            )
            response.status_code = 400
            return response


@require_http_methods([ "GET", "DELETE"])
def api_show_sales(request, id):
    if request.method == "GET":
        try:
            sales = Sale.objects.get(id=id)
            return JsonResponse(
                sales,
                encoder=SalesDetailEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse(
                {"message": "Does not exist"}
                )
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count>0})
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
