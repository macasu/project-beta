# Generated by Django 4.0.3 on 2023-10-26 08:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0004_alter_salesperson_employee_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salesperson',
            name='first_name',
            field=models.CharField(max_length=200),
        ),
    ]
