import {useEffect, useState} from 'react';

// works now
function AutomobileList() {

    const [automobiles, setAutomobiles] = useState([])

    async function getAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok){
            const data = await response.json();
            console.log('this is the data', data)
            setAutomobiles(data.autos)
        } else {
            console.log('getting an error ')
        }
    };
    useEffect(()=>{
        getAutomobiles();
    },[])

    return (
        <div>
            <button className="btn btn-primary my-3">Automobile List</button>
            <h1>Automobiles</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {automobiles.map(automobile => {
                return (
                    <tr key={automobile.id}>
                        <td> {automobile.vin}</td>
                        <td> {automobile.color}</td>
                        <td> {automobile.year}</td>
                        <td> {automobile.model.name}</td>
                        <td> {automobile.model.manufacturer.name}</td>
                        <td> {automobile.sold}</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
    </div>
    )
}
export default AutomobileList
