import React, { useState, useEffect } from 'react';

function AppointmentForm() {

    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date_time, setDateTime]  = useState('');
    const [technician_id, setTechnician]  = useState('');
    const [reason, setReason]  = useState('');
    const [technicians, setTechnicians]  = useState([]);





    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = date_time;
        data.technician_id = technician_id;
        data.reason = reason;
        console.log(data);



        const aptUrl = "http://localhost:8080/api/appointments/";
        const fetchApt = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }


    const response = await fetch(aptUrl, fetchApt);
    console.log('Response:', response);

    if (response.ok) {
        const newApt= await response.json();
        console.log(newApt);

        setVin('');
        setCustomer('');
        setDateTime('');
        setTechnician('');
        setReason('');
        window.location.reload()
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
    fetchData();
    }, []);




    const handleVinChange= async (event) => {
        const { value } = event.target;
        setVin(value);
    }

    const handleCustomerChange= async (event) => {
        const { value } = event.target;
        setCustomer(value);
    }
    const handleDateTimeChange= async (event) => {
        const { value } = event.target;
        setDateTime(value);
    }
    const handleTechnicianChange= async (event) => {
        const { value } = event.target;
        setTechnician(value);
    }
    const handleReasonChange= async (event) => {
        const { value } = event.target;
        setReason(value);
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" className="form-control" />
                            <label>Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} value={date_time} placeholder="Date time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date Time</label>
                        </div>
                        <div className="form-floating mb-3">
                             <select onChange={handleTechnicianChange} value={technician_id} required id="technician" className="form-control">
                             <option value="">Choose Technician</option>
                             {technicians.map((technician) => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}> {technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                             <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                             <label htmlFor="color">Reason</label>
                         </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm
