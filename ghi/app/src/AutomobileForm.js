import React, { useState} from 'react';

// still needs to work
function AutoMobileForm(){
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color;
        data.year = year;
        data.vin = vin;
        console.log(data)

        const automobileUrl = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);
            setColor('');
            setYear('');
            setVin('');
            window.location.reload()
        }
    };

    const handleChangeColor = async (event) => {
        const { value } = event.target;
        setColor(value);
    }

    const handleChangeYear = async (event) => {
        const { value } = event.target;
        setYear(value);
    }

    const handleChangeVin = async (event) =>{
        const { value } = event.target;
        setVin(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
                <h1>Create an automobile</h1>
                <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChangeColor} value ={color} placeholder ="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="first name">Color</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChangeYear} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleChangeVin} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">Vin</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        )
    }
    export default AutoMobileForm
