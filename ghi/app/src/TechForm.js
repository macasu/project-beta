import React, { useState, useEffect } from 'react';

function TechForm() {

    const [employeeId, setEmployeeId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName]  = useState('');





    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.employee_id = employeeId;
        data.first_name = firstName;
        data.last_name = lastName;
        console.log(data);



    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };


    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
        const newTechnician= await response.json();
        console.log(newTechnician);

        setEmployeeId('');
        setFirstName('');
        setLastName('');
        window.location.reload()
        }
    }

    const handleEmployeeIdChange= async (event) => {
        const { value } = event.target;
        setEmployeeId(value);
    }

    const handleFirstNameChange= async (event) => {
        const { value } = event.target;
        setFirstName(value);
    }
    const handleLastNameChange= async (event) => {
        const { value } = event.target;
        setLastName(value);
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a technician</h1>
                    <form onSubmit={handleSubmit} id="create-technicians-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} value={employeeId} placeholder="Manufacturer" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="manufacturer">Employee ID</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="model_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="color">Last Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechForm
