import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredApts] = useState([]);
  const [searchVin, setSearchVin] = useState('');

  useEffect(() => {
    async function fetchData() {
      const response = await fetch('http://localhost:8080/api/appointments/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
        setFilteredApts(data.appointments);
      } else {
        console.error('An error occurred fetching the data');
      }
    }

    fetchData();
  }, []);

  const handleSearch = () => {
    const filtered = appointments.filter((appointment) =>
      appointment.vin.toLowerCase().includes(searchVin.toLowerCase())
    );
    setFilteredApts(filtered);
  };

  return (
    <div>
      <h1>Service History</h1>
      <div className="mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search by VIN"
          value={searchVin}
          onChange={(e) => setSearchVin(e.target.value)}
        />
        <button className="btn btn-primary" onClick={handleSearch}>
          Search
        </button>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>

        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.vip ? 'Yes' : 'No'}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
