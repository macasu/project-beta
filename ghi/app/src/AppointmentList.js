import React, { useState, useEffect } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [soldVins, setSoldVins] = useState([]);

  async function getAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    } else {
      console.error('An error occurred fetching the data');
    }
  }

  const cancelAppointment = async(id) => {
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({"status": "canceled"}),
      headers: {
          'Content-Type': 'application/json'
      }
    }
    const response = await fetch(appointmentUrl, fetchConfig)
    if (response.ok) {
      console.log("canceled")
      window.location.reload()
    }
  }

  const finishAppointment = async(id) => {
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({"status": "finished"}),
      headers: {
          'Content-Type': 'application/json'
      }
    }
    const response = await fetch(appointmentUrl, fetchConfig)
    if (response.ok) {
      console.log("finished")
      window.location.reload()
    }
  }

  async function getVins() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
        const { autos } = await response.json();
        const soldVins = autos.map((automobile) => automobile.vin)
        setSoldVins(soldVins)
    } else {
        console.error("Error getting VIP status")
    }
}


  useEffect(() => {
  getAppointments();
  getVins();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date and Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>

      <tbody>
        {appointments.filter((appointment) => appointment.status !== "finished" && appointment.status !== "canceled").map((appointment) => {
          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.vip ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
              <td>{appointment.reason}</td>
              <td>
                <button onClick={(e) => cancelAppointment(appointment.id)}>Cancel</button>
              </td>
              <td>
                <button onClick={(e) => finishAppointment(appointment.id)}>Finish</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;
