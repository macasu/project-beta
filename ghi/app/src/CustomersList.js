import {useEffect, useState} from 'react';
// works
function CustomersList() {

    const [customers, setCustomers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers');

        if (response.ok){
            const data = await response.json();
            console.log('this is the data', data)
            setCustomers(data.customers)
        } else {
            console.log('getting an error ')
        }
    };
    useEffect(()=>{
        getData()
    },[])

    return (
        <div>
        {/* <Link to="/salespeople/create"> */}
            <button className="btn btn-primary my-3">Add a customer</button>
        {/* </Link> */}
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone number</th>
                </tr>
            </thead>
            <tbody>
                {customers && customers.map(customer => {
                    return (
                        <tr key={customer.phone_number}>
                            <td> {customer.first_name}</td>
                            <td> {customer.last_name}</td>
                            <td> {customer.address}</td>
                            <td> {customer.phone_number}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    )
            }
export default CustomersList
