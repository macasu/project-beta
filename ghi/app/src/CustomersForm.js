import React, { useState} from 'react';

// works
function SalesPeopleForm(){
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name;
        data.last_name = last_name;
        data.address = address;
        data.phone_number = phone_number;
        console.log("Data", data)
        const customersUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        const response = await fetch(customersUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            window.location.reload()
        }
    };

    const handleFormChangeFirstName = async (event) => {
        const { value } = event.target;
        setFirstName(value);
    }

    const handleFormChangeLastName = async (event) => {
        const { value } = event.target;
        setLastName(value);
    }

    const handleFormChangeAddress = async (event) =>{
        const { value } = event.target;
        setAddress(value);
    }
    const handleFormChangePhoneNumber = async (event) =>{
        const { value } = event.target;
        setPhoneNumber(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
                <h1>Create a sales person</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeFirstName} value ={first_name} placeholder ="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first name">First name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last name">Last name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChangeAddress} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="Address">Address</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChangePhoneNumber} value={phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="Employee ID">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        )
    }
    export default SalesPeopleForm
