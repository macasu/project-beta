import { useState, useEffect } from 'react';

function TechList() {
    const [ technicians, setTechs] = useState([]);


    async function getTechs() {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const data = await response.json();
        setTechs(data.technicians);
      } else {
        console.error('An error occurred fetching the data');
      }
    }

    useEffect(() => {
      getTechs();
    }, [])


      return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Employee ID</th>
            </tr>
          </thead>

          <tbody>
            {technicians.map((technician) => {
              return (
                <tr key={technician.id}>
                  <td>{ technician.first_name }</td>
                  <td>{ technician.last_name }</td>
                  <td>{ technician.employee_id }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    }

    export default TechList;
