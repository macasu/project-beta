import {useEffect, useState} from 'react';
// not fully done
function SalesList() {

    const [sales, setSale] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales');

        if (response.ok){
            const data = await response.json();
            console.log('this is the data', data)
            setSale(data.sales)
        } else {
            console.log('getting an error ')
        }
    };
    useEffect(()=>{
        getData()
    },[])

    return (
        <div>
            <button className="btn btn-primary my-3">Add a sale</button>
        <h1> Sales </h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales && sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td> {sale.salesperson.employee_id}</td>
                            <td> {sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td> {sale.customer.first_name}{sale.customer.last_name}</td>
                            <td> {sale.automobile.vin}</td>
                            <td> {sale.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    )
            }
export default SalesList
