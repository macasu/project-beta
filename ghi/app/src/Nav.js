import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link"to="salespeople">Salespeople</NavLink>
            </li>
            <li>
              <NavLink className="nav-link"to="salespeople/create">Add a Salesperson </NavLink>
            </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/technicians/add">Add a Technician</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/customers">Customers</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/customers/create">Create a customer</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/sales">Sales</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/sales/create">Create a sale</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/appointments/create">Create a Service Appointment</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/appointments">Service Appointment</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/service-history">Service Appointment History</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/manufacturers/create">Create Manufacturers</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/models">Models</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/models/create">Create a Vehicle Model</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/automobiles">Models</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/automobiles/create">Create a Vehicle Model</NavLink>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
