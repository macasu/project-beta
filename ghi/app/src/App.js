import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPeopleList from './SalesPeopleList';
import CustomersList from './CustomersList';
import CustomersForm from './CustomersForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import SalesPeopleForm from './SalesPeopleForm';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutoMobileForm from './AutomobileForm';
import TechList from './TechList';
import TechForm from './TechForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelsList from './ModelsList';


function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople" element={<SalesPeopleList/>}>
            <Route index element={<SalesPeopleList/>} />
          </Route>
          <Route path="/salespeople/create" element={< SalesPeopleForm/>} />
          <Route path="/customers" element={<CustomersList/>} />
          <Route path="/customers/create" element={<CustomersForm/>} />
          <Route path="/sales" element={<SalesList/>} />
          <Route path="/sales/create" element={<SalesForm/>} />
          <Route path ="/models" >
            <Route path="create" element ={<VehicleModelForm />} />
          </Route>
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/new" element={<AutoMobileForm />} />
          <Route path="technicians">
            <Route path="" element={<TechList />} />
            <Route path="add" element={<TechForm/>} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList />} />
            <Route path="create" element={<AppointmentForm />} />
          </Route>
          <Route path="service-history">
            <Route path="" element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList />} />
            {/* <Route path="create" element={<ModelForm />} /> */}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;



// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import MainPage from './MainPage';
// import Nav from './Nav';
// import SalesPeopleList from './SalesPeopleList';
// import CustomersList from './CustomersList';
// import SalesList from './SalesList';
// import SalesPeopleForm from './SalesPeopleForm';

// function App() {
//   return (
//     <BrowserRouter>
//       <Nav />
//       <div className="container">
//         <Routes>
//           <Route path="/" element={<MainPage />} />
//           <Route path="/salespeople" element={<SalesPeopleList />}>
//             <Route path="/new" element={<SalesPeopleForm />} />
//           </Route>
//           <Route path="/customers" element={<CustomersList />} />
//           <Route path="/sales" element={<SalesList />} />
//         </Routes>
//       </div>
//     </BrowserRouter>
//   );
// }

// export default App;
