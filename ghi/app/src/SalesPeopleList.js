import {useEffect, useState} from 'react';
// works
function SalesPeopleList() {

    const [salespeople, setSalesPeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople');

        if (response.ok){
            const data = await response.json();
            console.log('this is the data', data)
            setSalesPeople(data.salespeople)
        } else {
            console.log('getting an error ')
        }
    };
    useEffect(()=>{
        getData()
    },[])

    return (
        <div>
        {/* <Link to="/salespeople/create"> */}
            <button className="btn btn-primary my-3">Add a salesperson</button>
        {/* </Link> */}
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salespeople.map(person => {
                    return (
                        <tr key={person.employee_id}>
                        <td key = {person.employee_id}>{person.employee_id}</td>
                        <td key = {person.first_name}>{person.first_name}</td>
                        <td key = {person.last_name}>{person.last_name}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
    )
            }
export default SalesPeopleList
