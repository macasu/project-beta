import React, { useState, useEffect} from 'react';
// works
function VehicleModelForm() {

    const [name, setName]  = useState('');
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer_id, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([])

    const fetchManufacturers = async () => {
        const manufacturerUrl = "http://localhost:8100/api/models/";

        const response = await fetch(manufacturerUrl)

        if (response.ok){
            const data = await response.json();
            console.log(data)
            setManufacturers(data.models);
        }
    }

    useEffect(() => {
        fetchManufacturers();
    },[])


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id =  manufacturer_id;
        console.log(data);

    const VehicleModelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
    console.log(JSON.stringify(data))


    const response = await fetch(VehicleModelUrl, fetchConfig);
    if (response.ok) {
        const newManufacturer= await response.json();
        console.log(newManufacturer);

        setName('');
        setPictureUrl('');
        setManufacturer('');
        //getModels();
        window.location.reload()
        } else {
            console.error('Request failed with status:', response.status);
        }
    }

    const handleNameChange= async (event) => {
        const { value } = event.target;
        setName(value);
    }

    const handlePictureChange= async (event) => {
        const { value } = event.target;
        setPictureUrl(value);
    }
    const handleManufacturerChange= async (event) => {
        const { value } = event.target;
        setManufacturer(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a vehicle model</h1>
                    <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} value={picture_url} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="name">Picture Url</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleManufacturerChange} value={manufacturer_id} required id="manufacturer" name="manufacturer" className="form-select">
                            <option value="">Choose a manufacturer...</option>
                            {manufacturers && manufacturers.map(models => {
                                return (
                                    <option key={models.id} value={models.manufacturer.id}> {models.manufacturer.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        {/* <select value={manufacturer} onChange={handleManufacturerChange} required type="text" name="manufacturer" id="manufacturer" className="form-select">
                        <option value="">Choose a manufacturer...</option>
                        {manufacturers && manufacturers.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.name}></option>
                            )
                        })}
                        </select> */}

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm


// import React, { useState, useEffect } from 'react';

// function ManufacturerForm() {

//     const [name, setName]  = useState('');


//     const handleSubmit = async (event) => {
//         event.preventDefault();
//         const data = {};
//         data.name = name;
//         console.log(data);



//     const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
//     const fetchConfig = {
//         method: "post",
//         body: JSON.stringify(data),
//         headers: {
//         'Content-Type': 'application/json',
//         },
//     };


//     const response = await fetch(manufacturerUrl, fetchConfig);
//     if (response.ok) {
//         const newManufacturer= await response.json();
//         console.log(newManufacturer);

//         setName('');
//         window.location.reload()
//         }
//     }

//     const handleNameChange= async (event) => {
//         const { value } = event.target;
//         setName(value);
//     }



//     return (
//         <div className="row">
//             <div className="offset-3 col-6">
//                 <div className="shadow p-4 mt-4">
//                     <h1>Create a manufacturer</h1>
//                     <form onSubmit={handleSubmit} id="create-shoes-form">
//                         <div className="form-floating mb-3">
//                             <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                             <label htmlFor="name">Name</label>
//                         </div>
//                         <button className="btn btn-primary">Create</button>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     );
// }

// export default ManufacturerForm
