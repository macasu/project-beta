import React, { useState, useEffect} from 'react';

// doesnt fully work yet
function SalesForm(){
    const [automobile, setAutomobile] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    // employee_id was employee_Id which was giving me a bad 400 request
    // make sure to match

    const fetchAutomobiles = async () => {
        const autoUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(autoUrl)
        if (response.ok) {
            const data = await response.json();
            console.log("Automobiles data:", data);
            console.log("automobiles", automobiles)
            setAutomobiles(data.automobiles)
        }
    }

    const fetchSalespeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleUrl)
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setSalespeople(data.salespeople)
        }
    }

    const fetchCustomers = async () => {
        const customersUrl = "http://localhost:8090/api/customers/";
        const response = await fetch(customersUrl)
        if (response.ok){
            const data = await response.json()
            console.log("customer data", data)
            console.log("customers", customer)
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchAutomobiles();
        fetchSalespeople();
        fetchCustomers();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;
        console.log("Data", data)


        const salesUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json",
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);
            setAutomobile("");
            setSalesperson("");
            setCustomer("");
            setPrice("");
            window.location.reload()

        }
    };

    const handleFormChangeAutomobile = async (event) => {
        const { value } = event.target;
        setAutomobile(value);
    }

    const handleFormChangeSalesperson = async (event) => {
        const { value } = event.target;
        setSalesperson(value);
    }

    const handleFormChangeCustomer = async (event) =>{
        const { value } = event.target;
        setCustomer(value);
    }
    const handleFormChangePrice = async (event) =>{
        const { value } = event.target;
        setPrice(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
                <h1>Create a sale</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                        <select onChange={handleFormChangeAutomobile} value={automobile} required id="automobile" name="automobile" className="form-control">
                        <option value="">Choose an automobile VIN...</option>
                        {automobiles && automobiles.map(autos => {
                            return (
                                <option key={autos.vin} value={autos.vin}> {autos.vin}</option>
                                )
                        })}
                        </select>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleFormChangeSalesperson} value={salesperson} required id="salesperson" name="salesperson" className="form-control">
                    <option value="">Choose a salesperson...</option>
                    {salespeople && salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}> {salesperson.first_name} {salesperson.last_name}</option>
                            )
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleFormChangeCustomer} value={customer} required id="customer" name="customer" className="form-control">
                    <option value="">Choose a customer...</option>
                    {customers && customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}> {customer.first_name} {customer.last_name}</option>
                            )
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChangePrice} value={price} placeholder="0" required type="text" name="price" id="price" className="form-control" />
                    <label htmlFor="Employee ID">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        )
    }
    export default SalesForm
