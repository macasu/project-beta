import { useState, useEffect } from 'react';

function ModelsList() {
    const [ models, setModels] = useState([]);


    async function getModels() {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        console.error('An error occurred fetching the data');
      }
    }

    useEffect(() => {
      getModels();
    }, [])


      return (
        <div style={{ textAlign: 'center' }}>
            <h1>Models</h1>
            <table className="table table-striped">
            <thead>
                <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map((model) => {
                return (
                    <tr key={model.id}>
                    <td>{ model.name }</td>
                    <td>{ model.manufacturer.name }</td>
                    <td>{ model.picture_url }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
      );
    }

    export default ModelsList;
