# CarCar

Team:

* James - Services
* Marcus  - Sales

In Order to Run this App
1) Fork Project Beta from Gitlab at https://gitlab.com/macasu/project-beta
2) Run the terminal and input git clone https://gitlab.com/macasu/project-beta.git
3) Enter the project directory, you will need to input the following in this order in the terminal(DOCKER app/ containers should be running as well):
    a) docker volume create beta-data
    b) docker-compose build
    c) docker-compose up
4) Please Utilize this link http://localhost:3000/ on your browser
docker

## Diagram
![Alt text](image.png)
## Service microservice
In the Service microservice there is an Automobile Value object model =AutomobileVO which consists of the properties import_href, vin, and sold. The Technician model is there as well, with the properties first_name,last_name, and employee_id. There is also an appointment model, which utilizes the properties date_time, customer, reason, status, vin, and technician that utilizes the tehcnician model as a foreignkey.
The poller is utilized to get the vin and the sold for the AutomobileVO, getting data from the automobile model.
## API Endpoints and Ports and URL
List technicians	GET	http://localhost:8080/api/technicians/
Create a technician	POST	http://localhost:8080/api/technicians/
Delete a specific technician	DELETE	http://localhost:8080/api/technicians/:id/
List appointments	GET	http://localhost:8080/api/appointments/
Create an appointment	POST	http://localhost:8080/api/appointments/
Delete an appointment	DELETE	http://localhost:8080/api/appointments/:id/
Set appointment status to "canceled"	PUT	http://localhost:8080/api/appointments/:id/cancel/
Set appointment status to "finished"	PUT	http://localhost:8080/api/appointments/:id/finish/
Ports that is used is 8080.
In order to utilize technicians please put this in insomnia for JSON response
{
	"first_name": "Jams",
	"last_name": "Nguyen",
	"employee_id":1 (note that this needs to have a number and not leave this blank)
}

which brings out something like this
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Jams",
			"last_name": "Nguyen",
			"employee_id": 2
		},
Delete will need to have the id for the url to delete.
In order to utilize. Please create this in order to input for appointment in INSOMNIA for the JSON responses
{
  "date_time": "2023-10-31T14:30:00",
  "reason": "Routine Maintenance",
  "status": "Scheduled",
  "vin": "1HGCM82633A123456",
  "customer": "JohnDoe",
  "technician_id": 1
}

Which gives this, but needs to have the id from the technician
		{
			"id": 5,
			"date_time": "2023-10-31T14:30:00+00:00",
			"reason": "Routine Maintenance",
			"status": "Scheduled",
			"vin": "1HGCM82633A123456",
			"customer": "JohnDoe",
			"technician": {
				"id": 1,
				"first_name": "Jams",
				"last_name": "Nguyen",
				"employee_id": 2
			},
			"vip": false
		}
	]

    Delete will need to have the id for the url to delete.

    Put will have this inputted or something like it
    {
  "date_time": "2023-10-31T14:30:00",
  "reason": "Routine Maintenance",
  "status": "Scheduled",
  "vin": "1HGCM82633A123456",
  "customer": "JohnDoe",
  "technician_id": 1
}
If it is for cancel and finished for the PUT method respectively this will happen.
{
	"id": 5,
	"date_time": "2023-10-31T14:30:00+00:00",
	"reason": "Routine Maintenance",
	"status": "canceled",
	"vin": "1HGCM82633A123456",
	"customer": "JohnDoe",
	"technician": {
		"id": 13,
		"first_name": "Jams",
		"last_name": "Nguyen",
		"employee_id": 2
	}
}
{
	"id": 5,
	"date_time": "2023-10-31T14:30:00+00:00",
	"reason": "Routine Maintenance",
	"status": "finished",
	"vin": "1HGCM82633A123456",
	"customer": "JohnDoe",
	"technician": {
		"id": 13,
		"first_name": "Jams",
		"last_name": "Nguyen",
		"employee_id": 2
	}
}

## Sales microservice
In the shoes microservice, we are using 4 different models. We have an AutomobileVO that references the automobile model from the inventory folder of the project. We also have a Salesperson, customer, and sale model as well. The Sale model references the 3 existing models that we had to create, which are the salesperson, customer, and automobile model to properly display information as the Sale model needs the data from those models for it to function.
# API Endpoints and URLS
List a salesperson                      GET http://localhost:8090/api/salespeople/
Create a salesperson                    POST    http://localhost:8090/api/salespeople/
Delete a specific salesperson           DELETE  http://localhost:8090/api/people/:id/
List a customer                         GET http://localhost:8090/api/customer/
Create a customer                       POST    http://localhost:8090/api/customer/
Delete a customer                       DELETE  http://localhost:8090/api/customer/:id/
List a sale                             Get http://localhost:8090/api/sales/
Create a sales                          POST    http://localhost:8090/api/sales/
Delete a specific sale                  DELETE  http://localhost:8090/api/sales/:id/
for customers please use:
{
        "first_name": "many",
        "last_name": "K",
        "address": "127 fas",
        "phone_number": "9512164211"
}
    which should return:
{
    "id": 1,
    "first_name": "many",
    "last_name": "K",
    "address": "127 fas",
    "phone_number": 951216421
}
for salesperson please use:
{
    "first_name": "Alex",
    "last_name": "D",
    "employee_id": "1"
}
    which should return:
{
    "id": 1,
    "first_name": "Alex",
    "last_name": "D",
    "employee_id": "1"
}
for sales please use:
{
    "salesperson": "1",
    "customer": "1",
    "automobile": "1C3CC5FB2AN120174",
    "price": "40000"
}
    whichh should return:
    "sales": [
        {
            "id": 1,
            "automobile": {
                "vin": "2C3CC5FB2AN120174",
                "sold": false
            },
            "salesperson": {
                "id": 1,
                "first_name": "Alex",
                "last_name": "P",
                "employee_id": "1"
            },
            "customer": {
                "id": 1,
                "first_name": "dany",
                "last_name": "K",
                "address": "127 fas",
                "phone_number": 9514
            },
            "price": "40000"
        },
for the inventory please use the following in insomnia:
    get manufacturer:
     {
  "name": "Honda"
    }
    should return:
    {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Honda"
    }
    get a automobile:
    {
    "color":"blue",
    "year": 2014,
    "vin": "2C3CC5FB2AN120174",
    "model_id": 1
    }
should return:
{
    "href": "/api/automobiles/2C3CC5FB2AN120174/",
    "id": 1,
    "color": "blue",
    "year": 2014,
    "vin": "2C3CC5FB2AN120174",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Dusk",
        "picture_url": "https://upload3.wikimedia.org",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "Honda"
        }
    },
    "sold": false
}
get a vehicle model:
{
"name":"Dusk",
"picture_url": "https://upload3.wikimedia.org",
"manufacturer_id": 1
}
should return:
{
    "href": "/api/models/1/",
    "id": 1,
    "name": "Dusk",
    "picture_url": "https://upload3.wikimedia.org",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Honda"
    }
}
